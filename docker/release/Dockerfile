#
FROM muccg/python-base:ubuntu14.04-2.7
MAINTAINER https://bitbucket.org/ccgmurdoch/yabi/

ENV DEBIAN_FRONTEND noninteractive

# Project specific deps
RUN apt-get update && apt-get install -y \
  git \
  libpcre3 \
  libpcre3-dev \
  libpq-dev \
  libssl-dev \
  libxml2-dev \
  libxslt1-dev \
  krb5-config \
  krb5-user \
  libkrb5-dev \
  libssl-dev \
  libsasl2-dev \
  libldap2-dev \
  && apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN env --unset=DEBIAN_FRONTEND

WORKDIR /app
RUN git clone --depth=1 --branch=9.5.1 https://bitbucket.org/ccgmurdoch/yabi.git .

WORKDIR /app/yabi
RUN pip install --process-dependency-links .

EXPOSE 9100 9101
VOLUME ["/data"]

RUN chmod +x /app/docker-entrypoint.sh

# Drop privileges, set home for ccg-user
USER ccg-user
ENV HOME /data
WORKDIR /data

ENTRYPOINT ["/app/docker-entrypoint.sh"]
CMD ["uwsgi"]
