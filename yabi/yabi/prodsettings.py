from settings import *

# -=-=-=-=-=-=-=-=-=-=-=-=-=-=-#

# Default debug mode is off, turn on for trouble-shooting
# DEBUG = False

# Default SSL on and forced, turn off if necessary
# SSL_ENABLED = True
# SSL_FORCE = True
# SESSION_COOKIE_SECURE = True
# CSRF_COOKIE_SECURE = True

# -=-=-=-=-=-=-=-=-=-=-=-=-=-=-#
